#include "ESP8266WiFi.h"
#include <cstring>
#include <iostream>

void parseCSV(String const& s, const char* k, void(*cb)(int, int))
{
    int index = 0;
    int startPos = s.indexOf(k);
    if (startPos != -1) {
        startPos += strlen(k);
        const char* cs = s.c_str();
        for (int coma = startPos - 1; coma >= 0; coma = s.indexOf(",", coma+1))
        {
            coma++;
            cb(index, atoi(cs+coma));
            index++;
        }
    }
}

int parseInt(String const& s, const char* k)
{
    int ret = -1;
    int pos = s.indexOf(k);
    if (pos != -1) {
        const char* cs = s.c_str();
        pos += strlen(k);
        ret = atoi(cs+pos);
    }
    return ret;
}

void cb(int index, int value)
{
    std::cout << index << "=>" << value << std::endl;
}

int main(int argc, char *argv[]) {

    std::cout << parseInt("toggle?id=2", "id=") << std::endl;
    std::cout << parseInt("toggle?id=1", "id=") << std::endl;
    std::cout << parseInt("toggle?id=12", "id=") << std::endl;
    std::cout << parseInt("toggle?id=22&dupa=jasio", "id=") << std::endl;
    std::cout << parseInt("toggle?id=33&hello=world", "id=") << std::endl;

    parseCSV("?xxx=1,2,3,4,5,6,5,4,3,2,1,&", "xxx=", &cb);

    return 0;
} /* end main() */
