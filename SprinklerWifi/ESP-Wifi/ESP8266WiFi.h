#pragma once

#include <string>
#include <iostream>
#include <time.h>

struct String
{
    String() = default;
    String(std::string const& s)
        : str(s)
    {
    }
    String(char const* s)
        : str(s)
    {
    }
    String(long v)
        : str(std::to_string(v))
    {
    }
    long length() const
    {
        return str.size();
    }
    const char* c_str() const
    {
        return str.c_str();
    }
    String substring(long start) const
    {
        return str.substr(start);
    }
    String substring(long start, long stop) const
    {
        return str.substr(start, stop-start);
    }
    long indexOf(char const* s, long pos) const {
        auto p = str.find(s, pos);
        if (p == std::string::npos) {
            return -1;
        }
        return p;
    }

    long indexOf(char const* s) const {
        auto p = str.find(s);
        if (p == std::string::npos) {
            return -1;
        }
        return p;
    }
    std::string str;
};
String operator+ (String const& lhs, String const& rhs)
{
    return String{};
}

struct SerialPort
{
    void begin(int boude) {}
    void println() {
        std::cout << '\n';
    }
    void println(String const& s) {
        print(s);
        println();
    }
    void print(String const& s) {
        std::cout << s.str;
    }
};

SerialPort Serial;

struct WiFiClient {
    operator bool() const
    {
        return true;
    }
    bool available() const {
        return true;
    }
    String readStringUntil(char delim)
    {
        return "GET /SPRINKLER HTTP/1.1";
    }
    void flush(){}
    size_t print(String const& l)
    {
        std::cout << l.str;
        return 0;
    }
    size_t println() {
        std::cout << '\n';
        return 0;
    }
    size_t println(String const& l) {
        print(l);
        println();
        return 0;
    }
    size_t write(const uint8_t* , size_t size) {
        return size;
    }

};

struct WiFiServer
{
    WiFiServer(int port) {}
    void begin() {}
    WiFiClient available() {
        return {};
    }

};
enum WIFIStatus {
    WL_CONNECTED
};
struct WIFI {
    String localIP()
    {
        return String{};
    }
    String softAPIP()
    {
        return "127.0.0.1";
    }
    void softAP(String const& ap_name, String const& ap_pass) {}
    void begin(String const& ssid, String const& pass) {}
    WIFIStatus status() const
    {
        return WL_CONNECTED;
    }
};

WIFI WiFi;

void delay(int ms) {}

int millis()
{
    static int ms = 0;
    return ms++;
}

static constexpr int LOW = 0;
static constexpr int HIGH = 1;
enum Direction {
    INPUT, OUTPUT
};
void pinMode(int, Direction) {}
void digitalWrite(int, int){}
#ifdef ARDUINO
void setup();
void loop();

int main() {

    setup();
    while(true) {
        loop();
    }
    return 0;
} /* end main() */
#endif
