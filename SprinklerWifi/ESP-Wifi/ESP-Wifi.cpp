#include "ESP8266WiFi.h"
#include "page.h"
#include <string.h>

// wifi setup data
const char* ssid = "ssid";
const char* password = "password";

const char* AP_Name = "ESP8266-Sprinkler";
const char* ap_passwd = "a good password";

WiFiServer server(80);
void setupWifiAP();

// the pin used for the sprinkler
int millis_per_sec = 1000;
int sprinklerPin = 2; // GPIO2
int timer;
long curr_millis;
long dest_millis;
int timer_remaining = 0;
int new_request = 0;

void setup()
{
    // start serial debugging
    Serial.begin(115200);
    delay(10);

    // set the output pins
    pinMode(sprinklerPin, OUTPUT);
    digitalWrite(sprinklerPin, HIGH);
    timer_remaining = 0;

    // start Wifi -- choose one
    setupWifiAP();
    // setupWifi();

    // Start the web server
    server.begin();
    Serial.println("Server started");

    // Print the IP address
    Serial.print("Use this URL to connect: ");
    Serial.print("http://");
    Serial.print(WiFi.localIP());
    Serial.println("/");
}

void parseString(String const& s, const char* k, void(*cb)(const char*, int))
{
    const char* cs = s.c_str();
    int pos = s.indexOf(k);
    int lastPos = -1;
    if (pos != -1) {
        pos += strlen(k);
        lastPos = s.indexOf("&", pos);
        if (lastPos == -1)
            lastPos = s.length();
        cb(cs + pos, lastPos - pos);
    }
}

int parseInt(String const& s, const char* k)
{
    int ret = -1;
    int pos = s.indexOf(k);
    if (pos != -1) {
        const char* cs = s.c_str();
        pos += strlen(k);
        ret = atoi(cs + pos);
    }
    return ret;
}

void parseCSV(String const& s, const char* k, void (*cb)(int, int))
{
    int index = 0;
    int startPos = s.indexOf(k);
    if (startPos != -1) {
        startPos += strlen(k);
        const char* cs = s.c_str();
        for (int pos = startPos; pos >= 0; pos = s.indexOf(",", startPos + 1)) {
            cb(index, atoi(cs + pos));
            index++;
        }
    }
}

void toggle(int id)
{
    // TODO
}
void setDuration(int id, int duration)
{
    // TODO
}
void setRepetition(int id, int repetition)
{
    // TODO
}
void setStartAt(const char* s, int len)
{
    // TODO
    // format: 18:00
}
void setCurrentTime(const char* s, int len)
{
    // TODO
    // format 2019-06-15T10:02:25.213Z
}
void loop()
{
    int value = HIGH;
    // check and turn them off if a timer expires...
    curr_millis = millis();
    if ((curr_millis > dest_millis) && (timer > 0)) {
        digitalWrite(sprinklerPin, HIGH);
        timer = 0;
        value = HIGH;
        Serial.println("turning sprinklers off at:");
        Serial.print(curr_millis);
        Serial.print(" > ");
        Serial.println(dest_millis);
    }

    // Check if a client has connected
    WiFiClient client = server.available();
    if (!client) {
        return;
    }

    // Wait until the client sends some data
    Serial.println("new client");
    while (!client.available()) {
        delay(1);
    }

    // Read the first line of the request
    String request = client.readStringUntil('\r');
    Serial.println(request);
    client.flush();

    // Match the request

    if (request.indexOf("/getStatus") != -1) {
        parseString(request, "time=", &setCurrentTime);
        // TODO set current uC time to time
#if 0
        /// TODO send JSON string like this
        /// BEGIN-JSON
        {
            "states": [
                {"i":0, "s": true, "d": 15, "r":24},
                {"i":1, "s": true, "d": 15, "r":24},
                {"i":2, "s": true, "d": 15, "r":24},
                {"i":3, "s": true, "d": 15, "r":24},
                {"i":4, "s": true, "d": 15, "r":24},
                // (...)
                {"i":5, "s": true, "d": 15, "r":24}
            ],
            "send": {"t": 21. "p": 1013, "h": 65},
            "times": {"curr": "12:54", "startat": "18:00"}
        }
        /// END-JSON
#endif
    } else if (request.indexOf("/toggle") != -1) {
        int id = parseInt(request, "id=");
        toggle(id);

    } else if (request.indexOf("/apply") != -1) {
        parseCSV(request, "dur=", &setDuration);
        parseCSV(request, "rep=", &setDuration);
        parseString(request, "startat=", &setStartAt);
    } else {
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html;charset=utf-8");
        client.println("Content-Encoding: gzip");
        client.print("Content-Length: ");
        client.println(sizeof(bin2c_index_html_gz));
        client.println(""); //  do not forget this one
        client.write(reinterpret_cast<const uint8_t*>(bin2c_index_html_gz), sizeof(bin2c_index_html_gz));
    }

    // Return the response
    delay(1);
    Serial.println("Client disonnected");
    Serial.println("");
}

void setupWifiAP()
{
    WiFi.softAP(AP_Name, ap_passwd);
}

void setupWifi()
{
    // Connect to WiFi network
    Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
}
