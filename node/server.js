'use strict';

const express = require('express');

// Constants
const PORT = 9191;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world\n');
});
var statuses = [
    {i:0, s:false, d:10, r:24},
    {i:1, s:false, d:10, r:24},
    {i:2, s:false, d:10, r:24},
    {i:3, s:false, d:10, r:24},
    {i:4, s:false, d:10, r:24},
    {i:5, s:false, d:10, r:24},
    {i:6, s:false, d:10, r:24},
    {i:7, s:false, d:10, r:24},
    {i:8, s:false, d:10, r:24},
    {i:9, s:false, d:10, r:24}
];
const sens =  {t: 21, p: 1013, h: 65};
const times = { curr: "12:42", startat: "18:00" };

var nextst = true;

app.get('/toggle', (req, res) => {
    res.writeHeader(200, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });

    if (req.query) {
        if (req.query.id) {
            const id=req.query.id;
            if ((id >= 0) && (id < statuses.length)) {
                statuses[id].s = ! statuses[id].s;
            }
        }
    }
    const msg = {stats: statuses, times: times, sens: sens };
    res.end(JSON.stringify(msg));
});
app.get('/getStatus', (req, res) => {
    res.writeHeader(200, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });
    const date = new Date();
    times.curr = date; 
    for (var i = 0; i < statuses.length; ++i)
    {
        const s = statuses[i].s;
        statuses[i].s = nextst;
        nextst = s;
    }

    const msg = {stats: statuses, times: times, sens: sens };
    res.end(JSON.stringify(msg));

});
app.get("/apply", (req, res) => {
    console.log(JSON.stringify(req.query));
    if (req.query) {
        if (req.query.rep) {
            const repetitions =req.query.rep ;
            const valarray = repetitions.split(',');
            valarray.forEach(function(e,i) {
                if ((i < statuses.length) && (e.length > 0)) {
                    statuses[i].r = parseInt(e);
                }
            });
        }
        if (req.query.dur) {
            const durations =req.query.dur ;
            const valarray = durations.split(',');
            valarray.forEach(function(e,i) {
                if ((i < statuses.length) && (e.length > 0)) {
                    statuses[i].d = parseInt(e);
                }
            });
        }
        console.log("query: ", JSON.stringify(req.query));
        if (req.query.startat != null) {
            times.startat = req.query.startat;
        }
        console.log("times: ", JSON.stringify(times));
    }
    res.writeHeader(200, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });
    const msg = {stats: statuses};
    res.end(JSON.stringify(msg));
});
app.get('/getSensors', (req, res) => {
    res.writeHeader(200, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    });
    res.end(JSON.stringify({sens: sens}));
});
try {
    app.listen(PORT, HOST);
} catch(e) {
    console.log("exc: " + e);
}
// console.log('Running on http://${HOST}:${PORT}');
